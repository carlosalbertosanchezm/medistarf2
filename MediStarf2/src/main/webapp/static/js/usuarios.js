
function loadData(){
    let request = sendRequest('personas/list', 'GET', '');
    let table = document.getElementById('users-table');
    table.innerHTML = "";
    request.onload = function(){

        let data = request.response;
        console.log(data);
        data.forEach((element) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idPersona}</th>
                    <td>${element.rolPersona}</td>
                    <td>${element.nombrePersona}</td>
                    <td>${element.apellidoPersona}</td>
                    <td>${element.emailPersona}</td>
                    <td>${element.direccionPersona}</td>
                    <td>${element.telefonoPersona}</td>

                    <td>
                        <button type="button" class="btn btn-success" onclick='window.location = "/form_usuarios.html?id=${element.idPersona}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUser(${element.idPersona})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="8">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadUser(idUsuario){
    let request = sendRequest('usuario/list/'+idpersona, 'GET', '')
    let rol = document.getElementById('user-rol')
    let nombres = document.getElementById('user-nombres')
    let apellidos = document.getElementById('user-apellidos')
    let usuario = document.getElementById('user-persona')
    let password = document.getElementById('user-password')
    let tipoident = document.getElementById('user-tipoiden')
    let numident = document.getElementById('user-numident')
    let direccion = document.getElementById('user-direccion')
    let ciudad = document.getElementById('user-ciudad')
    let email = document.getElementById('user-email')
    let telefono = document.getElementById('user-telefono')
    let fechalog = document.getElementById('user-fechalog')
    let id = document.getElementById('user-id')
    request.onload = function(){

        let data = request.response;
        rol.value=data.rolpersona
        nombres.value=data.nombrepersona
        apellidos.value=data.nombrepersona
        usuario.value=data.usuariopersona
        password.value = data.clave
        tipoident.value=data.tipoidentificacion
        direccion.value=data.direccionpersona
        ciudad.value=data.ciudadpersona
        email.value = data.usuario
        telefono.value = data.telefonopersona
        fechalog.value = data.fechalogging
        id.value = data.idpersona

        id.value = data.idpersona
        status.checked = data.activo
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteUser(idPersona){
    let request = sendRequest('personas/'+idPersona, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveUser(){
 let rol = document.getElementById('user-rol').value
 let nombres = document.getElementById('user-nombres').value
 let apellidos = document.getElementById('user-apellidos').value
 let usuario = document.getElementById('user-persona').value
 let password = document.getElementById('user-password').value
 let tipoident = document.getElementById('user-tipoiden').value
 let numident = document.getElementById('user-numident').value
 let direccion = document.getElementById('user-direccion').value
 let ciudad = document.getElementById('user-ciudad').value
 let email = document.getElementById('user-email').value
 let telefono = document.getElementById('user-telefono').value
 let fechalog = document.getElementById('user-fechalog').value
 let id = document.getElementById('user-id').value
 let data = {'idpersona': id,'rolpersona': rol,'nombrepersona': nombres, 'apellidopersona' : apellidos, 'direccionpersona' : direccion,
 'usuariopersona' : usuario, 'clavepersona' : password, 'tipoidentificacion' : tipoident, 'numeroidentificacion' : numident,
 'ciudadpersona' : ciudad, 'emailpersona' : email, 'direccionpersona' : direccion, 'telefonopersona' : telefono, 'fechalogging' : fechalog}
 let request = sendRequest('usuario/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'usuarios.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}
