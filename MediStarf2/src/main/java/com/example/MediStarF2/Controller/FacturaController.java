/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStarF2.Controller;

import com.example.MediStarF2.Services.FacturaService;
import com.example.MediStarF2.modelos.Factura;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Felipe G
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/facturas")
public class FacturaController {

    @Autowired
    private FacturaService facturaservice;

    @PostMapping(value = "/")
    public ResponseEntity<Factura> agregar(@RequestBody Factura factura) {
        Factura obj = facturaservice.save(factura);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Factura> eliminar(@PathVariable Integer id) {
        Factura obj = (Factura) facturaservice.findById(id);
        if (obj != null) {
            facturaservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Factura> editar(@RequestBody Factura factura) {
        Factura obj = facturaservice.findById(factura.getIdFactura());
        if (obj != null) {
            obj.setPersona(factura.getPersona());
            obj.setFacturaVenta(factura.getFacturaVenta());
            obj.setValorTotalFct(factura.getValorTotalFct());
            obj.setFechaFactura(factura.getFechaFactura());
            facturaservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Factura> consultarTodo() {
        return facturaservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Factura consultaPorId(@PathVariable Integer id) {
        return facturaservice.findById(id);
    }

}
