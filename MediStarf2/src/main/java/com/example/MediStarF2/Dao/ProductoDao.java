/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.MediStarF2.Dao;

import com.example.MediStarF2.modelos.Producto;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author XIOMARA
 */
public interface ProductoDao extends CrudRepository<Producto,Integer>  {
    
}
