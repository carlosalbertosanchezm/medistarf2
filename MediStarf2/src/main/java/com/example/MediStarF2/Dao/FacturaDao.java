/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.MediStarF2.Dao;

import com.example.MediStarF2.modelos.Factura;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author LENOVO
 */

/**
 *
 * @author Felipe G
 */
@Repository
public interface FacturaDao extends CrudRepository<Factura,Integer> {

}  

