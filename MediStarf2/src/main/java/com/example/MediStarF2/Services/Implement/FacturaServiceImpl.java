/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStarF2.Services.Implement;

import com.example.MediStarF2.Services.FacturaService;
import com.example.MediStarF2.Dao.FacturaDao;
import com.example.MediStarF2.modelos.Factura;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author LENOVO
 */

@Service
public class FacturaServiceImpl implements FacturaService {
@Autowired
private FacturaDao facturaDao;
@Override
@Transactional(readOnly=false)
public Factura save(Factura factura) {
return facturaDao.save(factura);
}

@Override
@Transactional(readOnly=false)
public void delete(Integer id) {
facturaDao.deleteById(id);
}
@Override
@Transactional(readOnly=true)
public Factura findById(Integer id) {
return facturaDao.findById(id). orElse(null);
}
@Override
@Transactional(readOnly=true)
public List<Factura> findAll() {
return (List<Factura>) facturaDao.findAll();
}
}


