/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStarF2.Services.Implement;

import com.example.MediStarF2.Services.PersonaService;
import com.example.MediStarF2.Dao.PersonaDao;
import com.example.MediStarF2.modelos.Persona;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author LENOVO
 */
@Service
public class PersonaServiceImpl implements PersonaService {
@Autowired
private PersonaDao personaDao;
@Override
@Transactional(readOnly=false)
public Persona save(Persona persona) {
return personaDao.save(persona);
}

@Override
@Transactional(readOnly=false)
public void delete(Integer id) {
personaDao.deleteById(id);
}
@Override
@Transactional(readOnly=true)
public Persona findById(Integer id) {
return personaDao.findById(id). orElse(null);
}
@Override
@Transactional(readOnly=true)
public List<Persona> findAll() {
return (List<Persona>) personaDao.findAll();
}
}

