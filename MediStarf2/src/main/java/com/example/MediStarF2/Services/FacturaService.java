/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.MediStarF2.Services;

import com.example.MediStarF2.modelos.Factura;
import java.util.List;

/**
 *
 * @author LENOVO
 */
public interface FacturaService {
    public Factura save(Factura factura);  // es la clase y el objeto
    public void delete(Integer id);
    public Factura findById(Integer id);
    public List<Factura> findAll();
}
