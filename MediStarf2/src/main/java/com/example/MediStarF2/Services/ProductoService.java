/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.MediStarF2.Services;

import com.example.MediStarF2.modelos.Producto;
import java.util.List;

/**
 *
 * @author XIOMARA
 */
public interface ProductoService {
    public Producto save(Producto producto);
    public void delete(Integer id);
    public Producto findById(Integer id);
    public List<Producto> findAll();
}
