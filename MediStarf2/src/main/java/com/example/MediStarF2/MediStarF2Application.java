package com.example.MediStarF2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediStarF2Application {

	public static void main(String[] args) {
		SpringApplication.run(MediStarF2Application.class, args);
	}

}
