/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStarF2.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import com.example.MediStarF2.modelos.Factura;

/**
 * farmaciaVirtual MediStartPharmacy
 *
 * @author Carlos Alberto Sánchez Medina
 */
// esta entidad siempre se deja pegada al nombre de la clase
// esto se necesita para indicar que la clase Transaccion es una entidad  ...*** con la tabla "categorias" // 
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transacciones")
public class Transaccion implements Serializable {
// autowire permite inyectar codigo Sql en java

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtransaccion")
    private Integer idTransaccion;

    @ManyToOne
    @JoinColumn(name = "fk_idfactura")
    private Factura factura;
// foreign key /se cambio a minuscula
    @ManyToOne
    @JoinColumn(name = "fk_idproducto")
    private Producto producto;  //se cambio a minusculas

    @Column(name = "cantidadsalida")
    private Integer cantidadSalida;

    @Column(name = "valorunitariovnt")
    private float valorUnitarioVnt;

}
