/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 * farmaciaVirtual MediStartPharmacy
 * Esta clase permite hacer el manejo integral de las Personas de la Farmacia.
 * En este documento en espanol se trato de evitar ls tildes y las enes   hahaha
 *
 * Criterios de variables
 *   solo letras a excepción de las llaves foraneas
 * a) Base de datos
 *  nombres de las tablas en plural y minusculas
 *  nombres de los atributos de las tablas PascalCase, dos palabras completas, si se requiere, para las palabras adicionales
 *  solo las tres primeras consonantes, ejemplo atributo   valor Unitario de Compra =  ValorUnitarioCmp
 *  en todas las variables se indica a que tabla pertenecen con la ultima palabra.
 *  las llaves foraneas se denominan así   Fk_NameAtributte    donde NameAtributte es el nombre de la primary key de la tabla referenciada.
 * b)Clases
 *   todas en singular PascalCase  ejemplo clase Persona
 * c) variables en Java
 *  todas empiezan con Minusculas,  camelCase   ejemplo  valorUnitarioCmp
 *
 * @author Carlos Alberto Sánchez Medina
 *
 * Que encontrará en este archivo
 * 1. Package and imports
 * 2. @ arrobas
 * 3. Atributtes
 * 5. Getters and Setters
 * 6. ToString
 */
/*
  1. Package and imports
 */
package com.example.MediStarF2.modelos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/*
* 2. @ arrobas 
 */
// esta entidad siempre se deja pegada al nombre de la clase
// esto se necesita para indicar que la clase Persona es una entidad con la tabla "listPersonas" de la base de datos// 
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "personas")
public class Persona implements Serializable {  // es para de SQL 

//    @Autowired // autowire permite inyectar codigo Sql en java
    /* 
* 3. Atributtes
     */
    @Id     // esto es unicamente para la llave primaria    primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY)    // esto es unicamente si la llave primaria es autoincremental
    @Column(name = "idpersona")  // este "IdPersona" es el nombre del campo en la base de datos
    private int idPersona;    // este "idPersona"   es el nombre del atributo en Java Netbeans
    @Column(name = "rolpersona")
    private String rolPersona;
    @Column(name = "nombrepersona")
    private String nombrePersona;
    @Column(name = "apellidopersona")
    private String apellidoPersona;
    @Column(name = "usuariopersona")
    private String usuarioPersona;
    @Column(name = "clavepersona")
    private String clavePersona;
    @Column(name = "tipoidentificacion")
    private String tipoIdentificacion;
    @Column(name = "numeroidentificacion")
    private String numeroIdentificacion;
    @Column(name = "direccionpersona")
    private String direccionPersona;
    @Column(name = "ciudadpersona")
    private String ciudadPersona;
    @Column(name = "emailpersona")
    private String emailPersona;
    @Column(name = "telefonopersona")
    private String telefonoPersona;
    @Column(name = "fechalogging")
    private Date fechaLogging; // que tipo de dato para fechas Date
// si hay una foreign key  entonces:
    // @ManyToOne     o @OneToOne  o 
    //JoinColumn(name="IdProducto"
    //private Producto fk_IdProducto    quiere decir que es un atributo tipo Clase Producto llamado Fk_IdProducto en la tbal personas 
    // private Estudiante fk_IdEstudiante  // es la forma de colocar la llave foranea  de la clase estudiante en el campo IdEstudiante
}  