
function loadData(){
    let request = sendRequest('producto/list', 'GET', '')
    let table = document.getElementById('products-table');
    table.innerHTML = "";
    request.onload = function(){

        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idproducto}</th>
                    <td>${element.nombreproducto}</td>
                    <td>${element.categoriaproducto}</td>
                    <td>${element.valorunitarioprd}</td>
                    <td>${element.cantidadinventario}</td>
                    <td>
                        <button type="button" class="btn btn-success" onclick='window.location = "/form_productos.html?id=${element.idproducto}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteProducto(${element.idproducto})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadProducto(idProducto){
    let request = sendRequest('producto/list/'+idproducto, 'GET', '')
    let name = document.getElementById('product-name')
    let categ = document.getElementById('product-categ')
    let vunit = document.getElementById('product-unit-value')
    let quantity = document.getElementById('product-quantity')
    let id = document.getElementById('product-id')
    request.onload = function(){

        let data = request.response
        id.value = data.idproducto
        name.value = data.nombreproducto
        categ.value = data.categoriaproducto
        vunit.value = data.valorunitarioprd
        quantity.value = data.cantidadinventario
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteProducto(idProducto){
    let request = sendRequest('producto/'+idproducto, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveProducto(){
    let name = document.getElementById('product-name').value
    let categ = document.getElementById('product-categ').value
    let vunit = document.getElementById('product-unit-value').value
    let quantity = document.getElementById('product-quantity').value
    let id = document.getElementById('product-id').value
    let data = {'idproducto': id,'nombreproducto':name,'categoriaproducto':categ , 'valorunitarioprd': vunit, 'cantidad':quantity }
    let request = sendRequest('producto/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'productos.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}
